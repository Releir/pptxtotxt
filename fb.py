import sys
import argparse
import re
from pptx import Presentation


def start_conversion(filename):

    result = open("res.txt",'w')

    pptx = Presentation(filename)
    for slides in pptx.slides:
        for sh in slides.shapes:
            if hasattr(sh, "text"):
                result.write(sh.text)
                result.write("\n" * 2)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('FILE')
    parser.add_argument(
        '--convert',
        help='Converts pptx to a txt file',
        action='store_true'
    )
    args = parser.parse_args()

    if args.convert:
        start_conversion(args.FILE)
    else:
        print('You must select an operation to use.')
        print('Run this program with [-h] for help.')
        sys.exit(1)


if __name__ == '__main__':
    main()
